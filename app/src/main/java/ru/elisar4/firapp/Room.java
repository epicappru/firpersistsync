package ru.elisar4.firapp;

import com.google.firebase.database.IgnoreExtraProperties;

/**
 * Created by elisar4 on 01/08/16.
 */
@IgnoreExtraProperties
public class Room {

    public String name;
    public String state;

    public Room() {
        // Default constructor required for calls to DataSnapshot.getValue(Room.class)
    }

    public Room(String name, String state) {
        this.name = name;
        this.state = state;
    }


}
