package ru.elisar4.firapp;

import com.google.firebase.database.IgnoreExtraProperties;

@IgnoreExtraProperties
public class Project {

    public String owner;
    public String name;
    public Double created;
    public Double updated;

    public Project() {
        // Default constructor required for calls to DataSnapshot.getValue(User.class)
    }

    public Project(String owner, String name, Double created, Double updated) {
        this.owner = owner;
        this.name = name;
        this.created = created;
        this.updated = updated;
    }




}
